import React , {Component} from "react";
import { BrowserRouter, Route, Link ,Redirect} from 'react-router-dom';
// import { browserHistory } from 'react-router';
import { Layout , Menu, Icon, Modal, List, Row, Col, Card} from 'antd';
import "./App.scss";


import HomeBase from "./pages/homeBase";
import TeachesPage from "./pages/teachesPage" ;
import CampsPage from "./pages/campsPage" ;
import ActivitysPage from "./pages/activitysPage";
import CouchesPage from "./pages/couchesPage";
import ActivitysCouchPage from './pages/activitysCouchPage';
import ProductPage from './pages/productPage.js';
import TrainingPage from './pages/trainingPage.js';
import JoinusPage from './pages/joinusPage.js';
import QuestionPage from './pages/questionPage.js';

import Detail from "./components/detail";
import CampDetail from "./components/campDetail";
import ActivityDetail from "./components/activityDetail";
import ActivityCoachDetail from "./components/activitiyCouchDetail";
import CouchDetail from './components/couchDetail';
import ProductDetail from './components/productDetail';
import TrainingDetail from './components/trainingDetail';
import DeviceDetail from "./components/deviceDetail";
import CouchRecruitDetail from "./components/couchRecruitDetail";
import CustomerRecruitDetail from "./components/customerRecruitDetail";
import WorkDetail from "./components/workDetail";

import HttpRequest from './util/httpRequest';

const {
  Sider
} = Layout;
const SubMenu = Menu.SubMenu;

class SiderDemo extends React.Component {
  state = {
    collapsed: false,
    modalVisible: true,
    level: 1,
    maskStyle: {
      'backgroundColor': 'rgba(47,48,48,1)'
    },
    bodyStyle: {
    },
    beforeRoute: {
      herf: '/',
      id: '',
      title: undefined
    },
    activeRoute: {
      herf: '/',
      id: '',
      title: undefined,
    },
    initList: [
      {
        "children": [
            {
                "herf": "/",
                "id": "info1",
                "title": "基本信息/Info"
            },
            {
                "herf": "/",
                "id": "info2",
                "title": "教学中心/Teaching Center"
            },
            {
                "herf": "/",
                "id": "info3",
                "title": "教学成果/Teaching Archive"
            },
            {
                "id": "device",
                "title": "教学设备/Equipements"
            }
        ],
        "id": "home",
        "title": "学院介绍/Introduction"
      },
      {
        "children": [
            {
                "id": "teaches",
                "title": "教学课程/Teaching Lessons"
            },
            {
                "id": "camps",
                "title": "夏冬令营/Camps"
            },
            {
                "id": "activityscouch",
                "title": "教练培训活动/Training Coach"
            },
            {
                "id": "activitys",
                "title": "活动与赛事/Activity & Match"
            }
        ],
        "id": "teach",
        "title": "教学内容/Teaching Contents"
      },
      {
        "id": "couches",
        "title": "教练介绍/Info Coach"
      },
      {
        "id": "products",
        "title": "网上商城/Products"
      },
      {
        "id": "trainings",
        "title": "网络学院/Trainings"
      },
      {
        "id": "joinus",
        "title": "加入我们/Join Us"
      }
    ]
  };

  rootSubmenuKeys = ['home', 'teach'];

  constructor(props) {
    super(props);
    this.teachlink = React.createRef();
  }
  componentWillMount() {
    let self = this;
    HttpRequest.getMenu().
    then(response => {
      let firstMenu = response.data[0];
      response.data.map((item, index) => {
        item.index = index;
      });
      const newState = Object.assign({}, self.state, {
        'dataList': response.data,
        'initList': response.data
      });
      self.setState(newState);
    })
    .catch(error => {
      console.log(error)
    });
  }
  /*card 选择点击后的事件*/
  handleChooseMenu = (index) => {
    let source = {};
    if(this.state.dataList[index].children){
      //点击有第一级有子菜单需要用户再选择
      source.level = 2;
      source.dataList = JSON.parse(JSON.stringify(this.state.initList[index].children));
      source.activeRoute = JSON.parse(JSON.stringify(this.state.dataList[index]));
    } else if (this.state.dataList[index].children === undefined && this.state.level == 1) {
      // 点击第一级没有孩子的菜单
      this.setModalVisible(false);
      source.level = 1;
      source.activeIndex = index;
      source.activeRoute = JSON.parse(JSON.stringify(this.state.dataList[index]));
    } else {
      // 点击第二层菜单
      this.setModalVisible(false);
      source.level = 3;
      source.activeIndex = index;
      source.activeRoute = JSON.parse(JSON.stringify(this.state.dataList[index]));
      document.getElementById(this.state.dataList[index].id).click();
    }

    this.setState(source);
  }

  /*子页面回主页面*/
  handleBack = () => {
    let urlArr = window.location.href.split('/');
    // 一下为触发菜单的显示
    let level = this.state.level;
    for (var item of this.state.dataList) {
      if( window.location.href.indexOf(item.id) >=0 || (urlArr[3] == "" && urlArr.length === 4)) {
        this.setModalVisible(true);
        // 对于是第二级点击后的页面返回需要重新设置level
        if (this.state.level === 3) {
          this.setState({
            level: 2
          });
        }
      }
    }

    if(window.location.href.indexOf("device") >=0 ){
      this.setModalVisible(true);
    }
    // 以下为当前页面的跳转 course
    if (window.location.href.indexOf("course:") >=0 ){
      document.getElementById("teaches").click(); 
    }
    if(window.location.href.indexOf("activity:") >=0 ){
      document.getElementById("activitys").click();
    }
    if(window.location.href.indexOf("activitycouch:") >=0 ){
      document.getElementById("activityscouch").click();
    }
    if(window.location.href.indexOf("camp:") >=0 ){
      document.getElementById("camps").click();
    }
    if(window.location.href.indexOf("couch:") >=0 ){
      document.getElementById("couches").click();
    }
    if(window.location.href.indexOf("product:") >=0 ){
      document.getElementById("products").click();
    }
    if(window.location.href.indexOf("training:") >=0 ){
      document.getElementById("trainings").click();
    }
    if(window.location.href.indexOf("customerRecruit") >=0 || window.location.href.indexOf("couchRecruit") >=0 || window.location.href.indexOf("form") >=0 || window.location.href.indexOf("workDetail") >=0){
      document.getElementById("joinus").click();
    }
    
  }

  /*子菜单回主菜单*/
  handleBackMenu = () =>{
    this.setState({
      'level': 1,
      'dataList': this.state.initList
    });
  }


  onCollapse = (collapsed) => {
    this.setState({ collapsed });
  }

  /*支持锚点跳转*/
  scrollToAnchor = (anchorName) => {
    if (anchorName) {
      let anchorElement = document.getElementById(anchorName);
      if(anchorElement) { anchorElement.scrollIntoView(); }
    }
  }

  onClick = (openKeys) => {

    if(openKeys.keyPath.length === 1){
      this.setState({
        openKeys: [],
      });
    }else {
      if (openKeys.keyPath[0].indexOf('info') >=0 ){
        this.scrollToAnchor(openKeys.keyPath[0].replace('info', '00'));
      }
    }
  }

  setModalVisible(modalVisible) {
    this.setState({ modalVisible });
  }


  render() {

    return (
      <ion-app>
        <ion-nav>
          <ion-header>
            <ion-toolbar>
              <ion-buttons slot="start" onClick={this.handleBack.bind(this)}>
                <ion-back-button text=" "></ion-back-button>
              </ion-buttons>
              <ion-title>{this.state.activeRoute.title}</ion-title>
            </ion-toolbar>
          </ion-header>
          <ion-content >
            <Layout style={{ minHeight: '90vh', display : 'flex', flexDirection: 'column'}}>
             <Sider
                collapsible
                collapsed={this.state.collapsed}
                onCollapse={this.onCollapse}
              >
                <div className="logo" />
                <Menu forceSubMenuRender={true}  onClick={this.onClick.bind(this)} mode="inline">
                  {
                    this.state.initList.map((menu) => {
                      if (menu.children) {
                        return (
                          <SubMenu
                            key={menu.id}
                            id={menu.id}
                            title={<span><Icon type={menu.id} / ><span>{menu.title}</span></span>}
                          >{
                              menu.children.map((item) => {
                                if(item.herf) {
                                  return (
                                    <Menu.Item key={item.id}><Link to={item.herf} id={item.id}>{item.title}</Link></Menu.Item>
                                  )
                                } else {
                                  return (
                                    <Menu.Item key={item.id}><Link to={"/" + item.id} id={item.id}>{item.title}</Link></Menu.Item>
                                  )
                                }
                              })
                            }
                        </SubMenu>
                        )
                      } else {
                        return (
                          <Menu.Item key={menu.id} >
                            <Icon type={menu.id} />
                            <Link to={"/" + menu.id} id={menu.id}><span>{menu.title}</span></Link>
                          </Menu.Item>
                        )
                      }
                    })
                  }
                </Menu>
              </Sider>


              <Layout> 
                  <Route path="/teaches" exact component={TeachesPage}/>
                  <Route path="/camps" exact component={CampsPage}/>
                  <Route path="/activitys" exact component={ActivitysPage}/>
                  <Route path="/activityscouch" exact component={ActivitysCouchPage}/>
                  <Route path="/couches" exact component={CouchesPage}/>
                  <Route path="/products" exact component={ProductPage}/>
                  <Route path="/trainings" exact component={TrainingPage}/>
                  <Route path="/joinus" exact component={JoinusPage}/>
                  <Route path="/form" exact component={QuestionPage}/>
                  
                  <Route path="/couchRecruit" exact component={CouchRecruitDetail}/>
                  <Route path="/customerRecruit" exact component={CustomerRecruitDetail}/>
                  <Route path="/workDetail" exact component={WorkDetail}/>
                  <Route path="/device" exact component={DeviceDetail}/>
                  <Route path="/detail/course:id" exact component={Detail}/>
                  <Route path="/detail/camp:id" exact component={CampDetail}/>
                  <Route path="/detail/activity:id" exact component={ActivityDetail}/>
                  <Route path="/detail/activitycouch:id" exact component={ActivityCoachDetail}/>
                  <Route path="/detail/couch:id" exact component={CouchDetail}/>
                  <Route path="/detail/product:id" exact component={ProductDetail}/>
                  <Route path="/detail/training:id" exact component={TrainingDetail}/>

                  <Route path="/" exact component={HomeBase} />
                  <Redirect from="/info1" exact to="/" />
                  <Redirect from="/info2" exact to="/" />
                  <Redirect from="/info3" exact to="/" />
              </Layout>
              <Modal
                closable = {false}
                maskClosable = {false}
                footer = {null}
                maskStyle = {this.state.maskStyle}
                centered
                visible= {this.state.modalVisible}
                bodyStyle= {this.state.bodyStyle}
                className= "navigator"
                width = { window.innerWidth - 200}
              >
                {
                  this.state.level ===2 && <div className="tab modal" onClick={this.handleBackMenu.bind(this)}>
                    <Icon type="left" />
                    <div className='title'>{this.state.activeRoute.title}</div>
                  </div>
                } 
                <List
                grid={{ gutter: 16, column: 2 }}
                dataSource={this.state.dataList}
                renderItem={(item, index) => (
                  <List.Item>
                    <Row gutter={24}>
                      <Col span={24} onClick={this.handleChooseMenu.bind(this, index)}>
                        {
                          item.children &&  <Card className="menuCard" border = {false}>
                              <div className="link">
                                <span className="ch">{item.title.split('/')[0]}</span>
                                <br/>
                                <span className="en">{item.title.split('/')[1]}</span>
                              </div>
                            </Card>
                        }
                        {
                          item.children === undefined && <Link to={"/" + item.id}>
                            <Card className="menuCard" border = {false}>
                              <div className="link">
                                <span className="ch">{item.title.split('/')[0]}</span>
                                <br/>
                                <span className="en">{item.title.split('/')[1]}</span>
                              </div>
                            </Card>
                          </Link>
                        }
                      </Col>
                    </Row>
                  </List.Item>
                  )}
                />
              </Modal>
            </Layout>
          </ion-content>
        </ion-nav>
      </ion-app>
    )
  }
}

class App extends Component {
  // default State object
  state = {
    contacts: []
  };
  render() {
    return (
      <BrowserRouter>
        <SiderDemo />
      </BrowserRouter>
    );
  }
}

export default App;