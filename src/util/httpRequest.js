import axios from "axios";

 const Ip = "http://120.78.218.111:8003";
 const ConfigUrl = "/app/config";
 const CouchUrl = "/coach/";
 const ActivityUrl ="/activity/";
 // const CampUrl = "/activity/";
 const LessonUrl = "/lesson/";
 const ProductUrl = "/product/";
 const TrainingUrl = "/training/";
 const FromUrl = "/form/";
 const DeviceUrl = "/content/3";
 const CustomerRecruitUrl = "/content/1";
 const CouchRecruitUrl = "/content/2";
 const WorkUrl = '/content/4';

 const CouchsUrl = "/coachs";
 const ActivitysUrl = "/activitys";
 // const CampsUrl = "/activitys";
 const LessonsUrl = "/lessons/";
 const ProductsUrl = "/products";
 const TrainingsUrl = "/trainings/";

 const HttpRequest = {
 	getMenu: function() {
 		return axios.get(`${Ip}${ConfigUrl}?timestamp=${new Date().getTime()}`);
 	}, 
 	getCustomerRecruit: function () {
 		return axios.get(`${Ip}${CustomerRecruitUrl}?timestamp=${new Date().getTime()}`);
 	},
 	getCouchRecruit: function () {
 		return axios.get(`${Ip}${CouchRecruitUrl}?timestamp=${new Date().getTime()}`);
 	},
 	getWork: function() {
 		return axios.get(`${Ip}${WorkUrl}?timestamp=${new Date().getTime()}`);
 	}, 
 	getDevice: function () {
 		return axios.get(`${Ip}${DeviceUrl}?timestamp=${new Date().getTime()}`);
 	},
 	getCouchById: function (id) {
 		return axios.get(`${Ip}${CouchUrl}${id}?timestamp=${new Date().getTime()}`);
 	},
 	// 赛事与活动详情
 	getActivityById: function (id) {
 		return axios.get(`${Ip}${ActivityUrl}${id}?activity_type=match&timestamp=${new Date().getTime()}`);
 	},
 	// 冬/夏令营详情
 	getCampById: function (id) {
 		return axios.get(`${Ip}${ActivityUrl}${id}?activity_type=camp&timestamp=${new Date().getTime()}`);
 	},
 	// 教练活动详情
 	getActivityCoachById: function (id) {
 		return axios.get(`${Ip}${ActivityUrl}${id}?activity_type=activity_coach&timestamp=${new Date().getTime()}`);
 	},
 	// 课程详情
 	getLessionById: function (id) {
 		return axios.get(`${Ip}${LessonUrl}${id}?&timestamp=${new Date().getTime()}`);
 	}, 
 	// 产品详情
 	getProductById: function (id) {
 		return axios.get(`${Ip}${ProductUrl}${id}?&timestamp=${new Date().getTime()}`);
 	},
 	// 网络课程详情
 	getTrainingById: function (data) {
 		return axios.get(`${Ip}${TrainingUrl}${data}?timestamp=${new Date().getTime()}`);
 	},
 	getFromById: function(data){
 		return axios.get(`${Ip}${FromUrl}${data}?timestamp=${new Date().getTime()}`);
 	},

 	// 教练列表
 	getCouchs: function (data) {
 		return axios.get(`${Ip}${CouchsUrl}${data}&timestamp=${new Date().getTime()}`);
 	},
 	// 赛事与活动列表
 	getActivitys: function (data) {
 		return axios.get(`${Ip}${ActivitysUrl}${data}&activity_type=match&timestamp=${new Date().getTime()}`);
 	},
 	// 夏/冬令营列表
 	getCamps: function (data) {
 		return axios.get(`${Ip}${ActivitysUrl}${data}&activity_type=camp&timestamp=${new Date().getTime()}`)
 	},
 	// 教练活动列表
 	getActivityCoach: function(data) {
 		return axios.get(`${Ip}${ActivitysUrl}${data}&activity_type=activity_coach&timestamp=${new Date().getTime()}`)
 	},
 	// 课程列表
 	getLessions: function (data) {
 		return axios.get(`${Ip}${LessonsUrl}${data}&timestamp=${new Date().getTime()}`);
 	},
 	// 产品列表
 	getProducts: function (data) {
 		return axios.get(`${Ip}${ProductsUrl}${data}&timestamp=${new Date().getTime()}`);
 	},
 	// 网络课程 
 	getTrainings: function (data) {
 		return axios.get(`${Ip}${TrainingsUrl}${data}&timestamp=${new Date().getTime()}`);
 	},

 	//设置表单
 	setForm: function (data){
 		return axios({
		    method: 'post',
		    url: `${Ip}/form_values/`,
		    data: data
		})
 	}
 };

 export default HttpRequest;