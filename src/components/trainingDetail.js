import React, {Component} from "react";
import {Layout, Button, Spin, Carousel} from 'antd';
import HttpRequest from '../util/httpRequest';
import '../pages/homeBase.scss';
import '../pages/teachesPage.scss';
import './detail.scss';
import './productDetail.scss';

const {
  Content,
} = Layout;

export default class TrainingDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  getDetail(id) {
    let self = this;
    HttpRequest.getTrainingById(id)
    .then(response => {
  		const newState = Object.assign({}, self.state, {'detail': response.data.result});
  		self.setState(newState);
    })
    .catch(error => {
      console.log(error)
    });
  }
  refresh(){
    this.getDetail()
  }
  componentWillMount(){
  	// 获取详情页信息
  	this.getDetail(this.props.match.params.id.replace(":", ""));
  }
  componentWillUnmount(){
    this.setState({
      detail: false
    })
  }
  render(){
  	if (this.state && this.state.detail) {
      const imgElements = [] 
      let i = 0;
      if (typeof this.state.detail.cover_images === 'string') {
        let item = this.state.detail;
        item.cover_images = [item.cover_images];
        this.setState({
          detail: item
        });
      }
      if(this.state.detail.cover_images){
        for (let url of this.state.detail.cover_images) {
          imgElements.push( 
            <div key={i}>
              <img alt="背景图片"  src={url}/>
            </div>
          )
          i++;
        }
      } else if(this.state.detail.cover_image) {
         imgElements.push( 
            <div>
              <img alt="背景图片"  src={this.state.detail.cover_image}/>
            </div>
          )
      }

  		return (
        <div className="training detail">
          <div className="header">
            <Carousel effect="fade">
              {imgElements}
            </Carousel>
          </div>
          <div className="content teaches">
            <Content>
              <div dangerouslySetInnerHTML={{__html: this.state.detail.content}} />
            </Content>
          </div>
        </div>
	  	);
  	} else if(this.state && this.state.detail === false){
      return (
        <div className="loading">
          <Button type="primary" onClick={this.refresh}>点击刷新</Button>
        </div>
      );
  	} else {
      return (
        <div className="loading"><Spin size="large" /></div>
      );
    }
  	
  }
}