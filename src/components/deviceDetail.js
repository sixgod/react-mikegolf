import React, {Component} from "react";
import {Layout, Button, Spin, Carousel, Modal} from 'antd';
import HttpRequest from '../util/httpRequest';
import './detail.scss';

const {
  Content,
} = Layout;

function info() {
  Modal.info({
    title: '敬请期待',
    content: (
      <div>
        <p>预定/购买功能还在开发中...</p>
      </div>
    ),
    onOk() {},
  });
}


export default class DeviceDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  getDetail(id) {
    let self = this;
    HttpRequest.getDevice()
    .then(response => {
  		const newState = Object.assign({}, self.state, {'detail': response.data.result});
  		self.setState(newState);
    })
    .catch(error => {
      console.log(error)
    });
  }
  refresh(){
    this.getDetail()
  }
  componentWillMount(){
  	// 获取详情页信息
  	this.getDetail();
  }
  componentWillUnmount(){
    this.setState({
      detail: false
    });
    // 描述和title
    // <div className="title" dangerouslySetInnerHTML={{__html: this.state.detail.title}} />
    // <div className="desc" dangerouslySetInnerHTML={{__html: this.state.detail.desc}} />
  }
  render(){
  	if (this.state && this.state.detail) {
      const imgElements = [] 
      let i = 0;
      for (let url of this.state.detail.cover_images) {
        imgElements.push( 
          <div key={i}>
            <img alt="背景图片"  src={url}/>
          </div>
        )
        i++;
      }
  		return (
        <div className="detail device">
          <div className="header">
            <Carousel effect="fade">
              {imgElements}
            </Carousel>
          </div>
          <div className="content teaches">
            <Content>
              <div dangerouslySetInnerHTML={{__html: this.state.detail.content}} />
            </Content>
          </div>
        </div>
	  	);
  	} else if(this.state && this.state.detail === false){
      return (
        <div className="loading">
          <Button type="primary" onClick={this.refresh}>点击刷新</Button>
        </div>
      );
  	} else {
      return (
        <div className="loading"><Spin size="large" /></div>
      );
    }
  	
  }
}