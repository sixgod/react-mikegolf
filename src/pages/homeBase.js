import React , {Component} from "react";
import { Layout, Card, Col, Row, Carousel} from 'antd';
import './homeBase.scss';

const {
 Content,
} = Layout;

/*这是学院简介*/
export default class homeBase extends Component {
  componentDidMount() {
    let selectNode = document.querySelector('.ant-menu-item-selected')
    if (selectNode) {
      console.log(14, selectNode.innerText.trim())
      switch (selectNode.innerText.trim()) {
        case '教学中心':
          this.scrollToAnchor('002');
          break;
        case '教学成果':
          this.scrollToAnchor('003');
          break;
        case '教学设备':
          this.scrollToAnchor('004');
          break;
        default:
          this.scrollToAnchor('001');
          break;
      }
    }
  }
  componentWillUnmount(){
  }
  scrollToAnchor = (anchorName) => {
    if (anchorName) {
        let anchorElement = document.getElementById(anchorName);
        if(anchorElement) { anchorElement.scrollIntoView(); }
    }
  }
  render() {
  	return (
      <div className="HomeBase">
        <div className="topheader"  name="001" id="001">
          <div className="title">
            <h3 className='logotxt'>- MIGA -</h3>
            <h1 className='titletxt'>一流教学，成就高尔夫明日之星</h1>
            <h4 className='startxt'>◆ ◆ ◆</h4>
          </div>
          <article className="content">
            <section>迈克国际高尔夫学院由现任中国高尔夫国家队技术顾问、前中国国奥队女队主教练、中国顶级高尔夫教练之一Michael Dickie出任院长，特聘Andy Griffiths、Lee Elliott、Travis Howser、Joe Hoblyn、Sam Green多位国际PGA教练领衔执教。</section>
            <section>学院2014年率先引进TRACKMAN雷达追踪系统、AMM 3D电磁式位置追踪系统、SAM PuttLab室内推杆分析仪及V1 Pro视频分析软件；次年又从国外采购回SWING CATALYST平衡测试系统；2016和2017年又重金购买了S2M平衡板测试仪、4D Motion挥杆感应分析系统等全球领先的设备，2017年更是与Harold Swash创始人Phil Kenyon合作，引进VISIO这个在高尔夫业内广为知晓的器材品牌，使我院成为中国唯一一家代理其产品的机构；学院的大投入目的是为学员提供更专业的测试与评估，量身定制教学内容，提供不同层级的训练方案。</section>
            <section>迈克国际高尔夫学院致力于“一流教学，成就高尔夫明日之星”的办学宗旨。学院独创六位一体整合教学法、青少年五段评定，从运动学、体育学的基本原理出发，将高尔夫理论、测试评估和教学实践等有机结合，致力于将高尔夫教学规范化、整体化、科学化、专业化，发展为华东地区乃至全国最具影响力的高尔夫专业教学机构之一。</section>
            <section>MIGA得到社会各界的认同，曾与包玉刚实验学校、德威英国国际学校等全上海最优秀的学校签署教学合作计划。另外，MIGA是金山高尔夫球队指定唯一教学团队，致力为上海市培养和选拔高尔夫人才而不懈努力。</section>
          </article>
        </div>
        <Content>
      	  <article className="baseinfo">
      	  	<section className="cardWrapper background-white"  name="002" id="002">
                <Row gutter={16}>
                  <Col span={12}>
                    <Card title="东镇教学中心" bordered={false} >
                      <p>学院教学总部设于上海东镇乡村高尔夫俱乐部内。该俱乐部位于上海市闵行区沪青平公路391弄87号，交通便利，并拥有专属停车场，已成功运营20余年。<br/>
                      俱乐部占地面积约7000平方米，拥有40个打位的练习场及300平方米推杆果岭、真草切杆区、沙坑组成的短杆训练中心。俱乐部内还设有中餐厅、西餐厅、球具专卖店、咖啡厅、更衣淋浴室…<br/>
                      学院作为该俱乐部内唯一的高尔夫教学机构，配备8根教学专用球道、视频分析教学区、体能训练室。</p>
                      <span className="address">地址：上海市闵行区沪青平公路391弄87号(近航新路)<br/>
                      电话：158 2112 5969</span>
                    </Card>
                  </Col>
                  <Col span={12}>
                    <div className="image">
                      <Carousel effect="fade">
                        <div><img alt="背景图片"  className='image' src={require('../img/content1.png')} /></div>
                        <div><img alt="背景图片"  className='image' src={require('../img/content1_1.png')} /></div>
                        <div><img alt="背景图片"  className='image' src={require('../img/content1_2.png')} /></div>
                      </Carousel>
                    </div>
                  </Col>
                </Row>
            </section>

            <section className="cardWrapper background-gray " >
                <Row gutter={16}>
                  <Col span={12}>
                    <div className="image">
                      <Carousel effect="fade">
                        <div><img alt="背景图片"  className='image' src={require('../img/content2.png')} /></div>
                        <div><img alt="背景图片"  className='image' src={require('../img/content2_1.png')} /></div>
                        <div><img alt="背景图片"  className='image' src={require('../img/content2_2.png')} /></div>
                      </Carousel>
                    </div>
                  </Col>
                  <Col span={12}>
                    <Card title="黄兴公园教学中心" bordered={false} >
                      <p>黄兴教学点位于上海最大的高尔夫练习场——黄兴全民体育公园仲益高尔夫练习场内，场内唯一的真草短杆练习区由本学院出资改造，独家经营；2楼999贵宾室内配备6个打位以及全球一流数字化教学设备。</p>
                      <span className="address">地址：上海市杨浦区国顺东路410号黄兴全民体育公园高尔夫2楼999贵宾室<br/>电话：189 1863 7975</span>
                    </Card>
                  </Col>
                </Row>
            </section>

            <section className="cardWrapper background-white">
                <Row gutter={16}>
                  <Col span={12}>
                    <Card title="棕榈滩教学中心" bordered={false} >
                      <p>棕榈滩教学中心坐落于奉贤棕榈滩海景高尔夫俱乐部内，球场占地2022亩，以全场7288码18洞72杆国际标准锦标赛级球场为主体，堪称自然生态型球场的典范。全新打造的由PeterThomson先生本人授权的中国唯一“签名会所”让大家体验到无时不在弥散高尔夫的优雅味道与罕见的传奇情境。教学中心内配置了Trackman雷达击球分析设系统、V1高尔夫教学分析系统、推杆分析系统等，国外资深高尔夫驻场教练将为大家带来最好的教学服务。</p>
                      <span className="address">地址：上海市奉贤区金汇塘路88号棕榈滩海景高尔夫俱乐部<br/>电话：138 1740 3678</span>
                    </Card>
                  </Col>
                  <Col span={12}>
                    <div className="image">
                        <Carousel effect="fade">
                          <div><img alt="背景图片"  className='image' src={require('../img/content3.png')} /></div>
                          <div><img alt="背景图片"  className='image' src={require('../img/content3_1.png')} /></div>
                          <div><img alt="背景图片"  className='image' src={require('../img/content3_2.png')} /></div>
                        </Carousel>
                    </div>
                  </Col>
                </Row>
            </section>

            <section className="cardWrapper background-gray"  name="003" id="003">
                <Row gutter={16}>
                  <Col span={12} style = {{textAlign: "center"}}>
                    <div className='image-warpper top'>
                      <img alt="背景图片"  src={require('../img/profile1.png')} className="image"/>
                    </div>
                  </Col>
                  <Col span={12}>
                    <Card title="六位一体，整合教学" bordered={false} >
                      <p>体能状态、装备配备、技术掌握、练习程度、战术制定、心理承受能力是影响高尔夫成绩的几大关键要素。迈克国际高尔夫学院运用最先进的设备和仪器对每位学员的六种素养进行专业测试评估，并制定出相应的学习训练计划，帮助学员整体提高。</p>
                    </Card>
                  </Col>
                </Row>
            </section>


            <section className="cardWrapper background-white"  name="004" id="004">
                <Row gutter={16}>
                  <Col span={12}>
                    <Card title="六阶培养 五段评定" bordered={false} >
                      <p>迈克国际高尔夫学院已培养出数个青少年冠军，并综合全球先进教学经验，为青少年学员特别制定了高尔夫学习五步进阶计划，率先规范青少年培训标准。<br/>学院将根据青少年学员年龄及专业测试的结果来确定其训练级别；学员可选择团体训练或一对一特训，接受包括高尔夫礼仪、规则、体能、技能、下场战略战术、心理等各方面的指导和培训；通过技能考核后方能进入下一级别的学习；这个根据青少年年龄、能力量身定制的重要计划将有效帮助青少年循序渐进地掌握高尔夫各项技能，更可带领他们向职业高尔夫球手方向发展！</p>
                      <br/>
                      <p>★系统全面五步进阶计划<br/>★阶梯式教学目标<br/>★初学者至精英球手的金字塔教学结构<br/>★愉悦的教学过程激发参赛兴趣<br/>★培养高尔夫运动能力<br/>★培养运动精神，提高综合素养</p>
                    </Card>
                  </Col>
                  <Col span={12}  style = {{textAlign: "center"}}>
                    <div className='image-warpper'>
                      <img alt="背景图片"  src={require('../img/profile2.png')} className="image"/>
                    </div>
                  </Col>
                </Row>
            </section>

            <section className="cardWrapper background-black text-white">
                <h2 className="text-white">Aim Point 果岭判读 独门神技</h2>
                <Row gutter={16}>
                  <Col span={14} offset={5}>
                    <Card title="1个“神技”，6大收获" bordered={false} className="text-white">
                      <ul className="text-white">
                        <li>理解坡向及坡度是怎样影响高尔夫球运行的</li>
                        <li>研判多变的果岭</li>
                        <li>准确判断果岭坡度</li>
                        <li>确定最合适的推打高尔夫球方法</li>
                        <li>确定清晰的推杆次数</li>
                        <li>通过综合判断把控挥杆或推杆力度，更好地掌控球速</li>
                      </ul>
                    </Card>
                  </Col>
                </Row>
            </section>

      	  </article>
        </Content>
        </div>
	)
  }
}