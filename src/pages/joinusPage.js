import React , {Component} from "react";
import { Link } from 'react-router-dom';
import {  Layout, Card, Col, Row,  List} from 'antd';
import './joinusPage.scss';
const {
  Content,
} = Layout;

const { Meta } = Card;

/*这是教学内容-在线商城*/

export default class JoinusPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeight: 0,
      hasMore: true,// 判断接口是否还有数据，通过接口设置
      dataList:[{
        herf: "/customerRecruit",
        title: "客服招聘",
        iconClass: "",
        cover_image: "../img/joinus_1.png"
      }, {
        herf: "/couchRecruit",
        title: "教练招聘",
        iconClass: "",
        cover_image: "../img/joinus_2.png"
      },{
        herf: "/form",
        title: "学院问卷",
        iconClass: "学员登记问卷",
        cover_image: "../img/joinus_3.png"
      },{
        herf: "/workDetail",
        title: "合作赞助",
        iconClass: "",
        cover_image: "../img/joinus_1.png"
      }
      ], // 数据列表
      count: 20, 
      offset: 0,
    };
  }

  componentWillUnmount(){
    this.setState({
      dataList: []
    })
  }

  render() {
    return (
      <div className="joinus">
        <div className="header"></div>
        <Content>
          <article className="shadow">
            <List
              grid={{ gutter: 16, column: 4 }}
              dataSource={this.state.dataList}
              renderItem={(item, index) => (
                <List.Item>
                  <Row gutter={24}>
                    <Col span={24}>
                      <Link to={item.herf}>
                        <Card>
                          <div className={'image'+index}></div>
                          <div className="link">
                            <Meta
                              title={item.title}
                            />
                          </div>
                        </Card>
                      </Link>
                    </Col>
                  </Row>
                </List.Item>
              )}
            />
          </article>
        </Content>
        </div>
  )
  }
}