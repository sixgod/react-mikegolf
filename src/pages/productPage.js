import React , {Component} from "react";
import { Link } from 'react-router-dom';
import HttpRequest from '../util/httpRequest';
import './productPage.scss';

import {  Layout, Card, Col, Row, Button, List, Spin} from 'antd';

const {
  Content,
} = Layout;

const { Meta } = Card;


/*这是教学内容-在线商城*/

export default class ProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrollHeight: 0,
      hasMore: true,// 判断接口是否还有数据，通过接口设置
      loading: true,
      dataList:[
      ], // 数据列表
      count: 20, 
      offset: 0,
    };
  }

  componentDidMount(){
    this.getCourseList();
  }

  componentWillUnmount(){
    this.setState({
      dataList: []
    })
  }

  // 处理滚动监听
  handleScroll(){
    const {hasMore} = this.state;
    if(!hasMore){
        return;
    }
    //下面是判断页面滚动到底部的逻辑
    if(this.scrollDom.scrollTop + this.scrollDom.clientHeight >= this.scrollDom.scrollHeight ){
      this.getCourseList()
    }
  }

  handleClick(index, event){
    event.stopPropagation();
    event.preventDefault();
  }

  getCourseList() {
    let now = new Date() * 1;
    let time = this.time;
    this.setState({
      loading: true
    });
    console.log(time, now);
    if(now && time && (now - time < 2000 ) && this.state.offset !== '0') {
      return;
    } else {
      this.time = now;
    }
    // getLessions
    let courseArray = this.state.dataList;
    let self = this;
    HttpRequest.getProducts(`?offset=${this.state.offset}&count=${this.state.count}`)
    .then(response => {
      
      for (let item of response.data.result) {
        item.status = false;
        item.herf = "/detail/product:" + item.id;
        item.subtitle = item.title.split('】')[1];
        item.classarr = "ant-row buyWapper buy";
        item.title = item.title.replace(item.subtitle, "");
        courseArray.push(item);
      }
      const newState = Object.assign({}, self.state, {
        'offset': self.state.offset + self.state.count,
        'dataList': courseArray,
        'loading': false
      });
      if(response.data.result.length < self.state.count){
        newState.hasMore = false;
      } 
      self.setState(newState);
    })
    .catch(error => {
      console.log(error)
      this.setState({
        loading: false
      });
    });
  }

  handleBuyCourse(index, event) {
    event.stopPropagation();
    event.preventDefault();
    let self = this;
    let dataList= self.state.dataList;
    dataList[index].classarr = "ant-row buyWapper";
    self.setState({'dataList':dataList}, () => {
      setTimeout(function (argument) {
        dataList[index].classarr = "ant-row buyWapper buy";
        self.setState({'dataList':dataList });
      }, 3000);
    }); 
  }

  render() {

    return (
      <div className="onlineshop" ref={body=>this.scrollDom = body}
                onScroll={this.handleScroll.bind(this)} >
        <div className="icon">
        </div>
        <Content>
          <article className="shadow">
            <List
              grid={{ gutter: 16, column: 3 }}
              dataSource={this.state.dataList}
              loading={this.state.loading}
              renderItem={(item, index) => (
                <List.Item>
                  <Row gutter={24}>
                    <Col span={24}>
                      <Link to={item.herf}>
                        <Card
                          cover={<img alt="背景图片"  src={item.cover_image} />}
                        >
                          <Meta
                            title={item.title}
                            description={'￥' + item.price}
                          />
                          <Button type="primary" onClick={this.handleBuyCourse.bind(this, index)}>立即购买</Button>
                          <Row gutter={24} className={item.classarr}>
                            <Col span={12}>
                              <a href={item.link.taobao}  onClick="this.handleClick.bind(this, index)"><Button type="primary"  onClick="this.handleClick.bind(this, index)">淘宝</Button></a>
                            </Col>
                            <Col span={12}>
                              <a href={item.link.weixin}  onClick="this.handleClick.bind(this, index)"><Button type="primary"  onClick="this.handleClick.bind(this, index)">微信</Button></a>
                            </Col>
                          </Row>
                        </Card>
                      </Link>
                    </Col>
                  </Row>
                </List.Item>
              )}
            >
            </List>
          </article>
        </Content>
        </div>
  )
  }
}